package es.slothdevelopers.slothframework.views;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.widget.ImageView;

import java.lang.reflect.Field;

import es.slothdevelopers.slothframework.injectors.SaveStateInViewInjector;
import es.slothdevelopers.slothframework.injectors.SingletonInjector;
import es.slothdevelopers.slothframework.utils.SlothLog;

public class SlothImageView extends ImageView{
    protected SlothLog log = new SlothLog(this);
    SingletonInjector singletonInjector = new SingletonInjector(this);
//    SaveStateInViewInjector saveStateInjector = new SaveStateInViewInjector(this);

    public SlothImageView(Context context) {
        super(context);
        parseFields();

    }

    public SlothImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseFields();
    }

    public SlothImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        parseFields();

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        singletonInjector.processSingletons(getContext().getApplicationContext());
    }

    private void parseFields() {
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            singletonInjector.checkIfFieldIsSingleton(field);
//            saveStateInjector.checkIfFieldToSaveState(field);
        }
    }



//    @Override
//    protected Parcelable onSaveInstanceState() {
//        super.onSaveInstanceState();
//        return saveStateInjector.getSavedElementsInParceable();
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Parcelable state) {
//        saveStateInjector.setSavedParceableElements(state);
//        super.onRestoreInstanceState(state);
//
//    }
}
