package es.slothdevelopers.slothframework.injectors;

import android.os.Bundle;
import android.os.Parcelable;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

import es.slothdevelopers.slothframework.annotations.SaveState;

public class SaveStateInViewInjector {
    Object caller;
    List<Field> saveFieldParameters = new LinkedList<Field>();

    public SaveStateInViewInjector(Object caller) {
        this.caller = caller;
    }
    public void checkIfFieldToSaveState(Field field) {
        if (field.isAnnotationPresent(SaveState.class)){
            saveFieldParameters.add(field);
        }
    }

    private void storeField(Object object, Field field, Bundle savedInstanceState, String storeInBundlePrefix) throws Exception {
        if (field.getType().equals(String.class)){
            savedInstanceState.putString(storeInBundlePrefix + field.getName(), (String) field.get(object));
        }else if (field.getType().equals(int.class)){
            savedInstanceState.putInt(storeInBundlePrefix + field.getName(), field.getInt(object));
        } else if (field.getType().equals(boolean.class)){
            savedInstanceState.putBoolean(storeInBundlePrefix + field.getName(), field.getBoolean(object));
        } else if (field.getType().equals(long.class)){
            savedInstanceState.putLong(storeInBundlePrefix + field.getName(), field.getLong(object));
        } else if (field.getType().equals(double.class)) {
            savedInstanceState.putDouble(storeInBundlePrefix + field.getName(), field.getDouble(object));
        } else if (field.getType().equals(float.class)) {
            savedInstanceState.putFloat(storeInBundlePrefix + field.getName(), field.getFloat(object));
        } else if (field.getType().isEnum()){
            Enum<?> e = (Enum<?>) field.get(object);
            savedInstanceState.putString(storeInBundlePrefix + field.getName() + "name", e.name());
            savedInstanceState.putInt(storeInBundlePrefix + field.getName() + "ordinal", e.ordinal());
        } else if (implementsList(field.getType())) {
            storeListInBundle(field.get(object), field, savedInstanceState, storeInBundlePrefix + field.getName() + ".");
        } else{// we suppose it's a pojo
            for (Field objectField: field.get(object).getClass().getDeclaredFields()){
                objectField.setAccessible(true);
                storeField(field.get(object), objectField, savedInstanceState, storeInBundlePrefix + field.getName() + ".");
            }
        }
    }

    private  boolean implementsList(Class clazz){
        for (Class actualClazz : clazz.getInterfaces()){
            if (actualClazz.getName().equals("java.util.List")){
                return true;
            }
        }
        return false;
    }

    private void storeListInBundle(Object object, Field field, Bundle savedInstanceState, String storeInBundlePrefix) throws Exception {
        List<Object> list = (List<Object>) object;
        savedInstanceState.putInt(storeInBundlePrefix + "size", list.size());
        if (list.size()>0){
            Object element = list.get(0);
            savedInstanceState.putString(storeInBundlePrefix + "type", element.getClass().getCanonicalName());
        }
        long index = 0;
        for(Object element: list){
            for (Field elementField: element.getClass().getDeclaredFields()){
                elementField.setAccessible(true);
                storeField(element, elementField, savedInstanceState, storeInBundlePrefix + index + ".");
            }
            index++;
        }
    }


    public void restoreFields(Bundle savedInstanceState, String recoverFromBundlePrefix) {
        for (Field field: saveFieldParameters){
            try {
                restoreField(caller, field, savedInstanceState, recoverFromBundlePrefix);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void restoreField(Object caller, Field field, Bundle savedInstanceState, String restoreFromBundlePrefix) throws Exception {
        if (field.getType().equals(String.class)){
            field.set(caller, savedInstanceState.getString(restoreFromBundlePrefix + field.getName()));
        } else if (field.getType().equals(int.class)){
            field.setInt(caller, savedInstanceState.getInt(restoreFromBundlePrefix + field.getName()));
        } else if (field.getType().equals(boolean.class)){
            field.setBoolean(caller, savedInstanceState.getBoolean(restoreFromBundlePrefix + field.getName()));
        } else if (field.getType().equals(long.class)){
            field.setLong(caller, savedInstanceState.getLong(restoreFromBundlePrefix + field.getName()));
        } else if (field.getType().equals(double.class)) {
            field.setDouble(caller, savedInstanceState.getDouble(restoreFromBundlePrefix + field.getName()));
        } else if (field.getType().equals(float.class)) {
            field.setFloat(caller, savedInstanceState.getFloat(restoreFromBundlePrefix + field.getName()));
        } else if (field.getType().isEnum()){
            field.set(caller, Enum.valueOf((Class) field.getType(), savedInstanceState.getString(restoreFromBundlePrefix + field.getName() + "name")));
        } else if (implementsList(field.getType())) {
            Object obj = Class.forName(field.getType().getName()).newInstance();
            restoreListFromBundle(obj, savedInstanceState, restoreFromBundlePrefix + field.getName() + ".");
            field.set(caller, obj);
        } else {
            Object obj = Class.forName(field.getType().getName()).newInstance();
            for (Field objectField: field.getType().getDeclaredFields()){
                objectField.setAccessible(true);
                restoreField(obj, objectField, savedInstanceState, restoreFromBundlePrefix + field.getName() + ".");
            }
            field.set(caller, obj);
        }
    }

    private void restoreListFromBundle(Object object, Bundle savedInstanceState, String restoreFromBundlePrefix) throws Exception {
        List<Object> list = (List<Object>) object;

        int size = savedInstanceState.getInt(restoreFromBundlePrefix + "size");
        String type = savedInstanceState.getString(restoreFromBundlePrefix + "type");
        for(int i = 0; i < size; i++) {

            Object obj = Class.forName(type).newInstance();

            for (Field elementField: obj.getClass().getDeclaredFields()){
                elementField.setAccessible(true);
                restoreField(obj, elementField, savedInstanceState, restoreFromBundlePrefix + i + ".");
                list.add(obj);
            }
        }
    }

    public Parcelable getSavedElementsInParceable() {
        Bundle bundle = new Bundle();
        for (Field field: saveFieldParameters){
            try {
                storeField(caller, field, bundle, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return bundle;
    }

    public void setSavedParceableElements(Parcelable state) {
        restoreFields((Bundle) state, "");
    }
}
