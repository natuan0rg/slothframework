package es.slothdevelopers.slothframework.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.lang.reflect.Field;

import es.slothdevelopers.slothframework.SlothComponent;
import es.slothdevelopers.slothframework.activity.SlothActionBarActivity;
import es.slothdevelopers.slothframework.activity.SlothFragmentActivity;
import es.slothdevelopers.slothframework.injectors.InterfaceInjector;
import es.slothdevelopers.slothframework.injectors.SaveStateInjector;
import es.slothdevelopers.slothframework.injectors.SingletonInjector;
import es.slothdevelopers.slothframework.injectors.ViewInjector;
import es.slothdevelopers.slothframework.listeners.OnBackPressedListener;
import es.slothdevelopers.slothframework.utils.MessageUtils;
import es.slothdevelopers.slothframework.utils.SlothLog;

public class SlothDialogFragment extends DialogFragment implements OnBackPressedListener, SlothComponent {
    protected SlothLog log = new SlothLog(this);

    ViewInjector viewInjector = new ViewInjector(this);
    InterfaceInjector interfaceInjector = new InterfaceInjector(this);
    SingletonInjector singletonInjector = new SingletonInjector(this);
    SaveStateInjector saveStateInjector = new SaveStateInjector(this);

    public SlothDialogFragment(){
        parseFields();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        interfaceInjector.attachInterfacesToActivity(getActivity());
        interfaceInjector.attachInterfacesToParentFragment(getParentFragment());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            saveStateInjector.restoreFields(savedInstanceState, "");
        }
        singletonInjector.processSingletons(getActivity().getApplicationContext());

    }

    private void parseFields() {
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            interfaceInjector.checkIfFieldIsInterfaceToBeAttached(field);
            singletonInjector.checkIfFieldIsSingleton(field);
            saveStateInjector.checkIfFieldToSaveState(field);
            viewInjector.checkIfViewToInject(field);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            ((SlothFragmentActivity)getActivity()).subcribeOnBackPressed(this);
        } catch (ClassCastException e) {
            try {
                ((SlothActionBarActivity) getActivity()).subcribeOnBackPressed(this);
            } catch (ClassCastException cce){
                log.error(MessageUtils.checkParentIsSloth(getActivity().getClass().getSimpleName()));
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewInjector.injectViewList(getView());
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        saveStateInjector.storeFields(savedInstanceState);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public boolean onBackPressed() {
        if (getActivity() == null){
            return false;
        }

        try {
            SlothFragmentActivity fa = (SlothFragmentActivity) getActivity();
            fa.goBack();
            return true;
        } catch (ClassCastException e) {
            try {
                SlothActionBarActivity fa = (SlothActionBarActivity) getActivity();
                fa.goBack();
                return true;
            } catch (ClassCastException cce){
                log.error(MessageUtils.checkParentIsSloth(getActivity().getClass().getSimpleName()));
            }
        }
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            SlothFragmentActivity fa = (SlothFragmentActivity) getActivity();
            fa.unsubscribe(this);
        } catch (ClassCastException e) {
            try {
                SlothActionBarActivity fa = (SlothActionBarActivity) getActivity();
                fa.unsubscribe(this);
            } catch (ClassCastException cce){
                log.error(MessageUtils.checkParentIsSloth(getActivity().getClass().getSimpleName()));
            }
        }
    }

    @Override
    public Activity getSlothActivity() {
        return getActivity();
    }
}
