package es.slothdevelopers.slothframework.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import es.slothdevelopers.slothframework.SlothComponent;
import es.slothdevelopers.slothframework.annotations.SaveFragmentReference;
import es.slothdevelopers.slothframework.injectors.SaveStateInjector;
import es.slothdevelopers.slothframework.injectors.SingletonInjector;
import es.slothdevelopers.slothframework.injectors.ViewInjector;
import es.slothdevelopers.slothframework.listeners.OnBackPressedListener;
import es.slothdevelopers.slothframework.utils.SlothLog;

import static es.slothdevelopers.slothframework.utils.MessageUtils.checkAnnotationParameterIdMessage;
import static es.slothdevelopers.slothframework.utils.MessageUtils.checkAnnotationParameterIdMessageByTag;
import static es.slothdevelopers.slothframework.utils.MessageUtils.checkIfAnnotationParameterIsEmpty;

public class SlothActionBarActivity extends ActionBarActivity implements SlothComponent {
    protected SlothLog log = new SlothLog(this);
    List<Field>  saveFieldFragmentReferences = new LinkedList<Field>();
    List<OnBackPressedListener> onBackPressedListeners = new LinkedList<OnBackPressedListener>();
    ViewInjector viewInjector = new ViewInjector(this);
    SingletonInjector singletonInjector = new SingletonInjector(this);
    SaveStateInjector saveStateInjector = new SaveStateInjector(this);

    public SlothActionBarActivity(){
        parseFields();
    }

    private void parseFields() {
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            checkIfFieldToSaveFragmentReference(field);
            singletonInjector.checkIfFieldIsSingleton(field);
            saveStateInjector.checkIfFieldToSaveState(field);
            viewInjector.checkIfViewToInject(field);
        }
    }

    private void checkIfFieldToSaveFragmentReference(Field field) {
        if (field.isAnnotationPresent(SaveFragmentReference.class)){
            if (field.getAnnotation(SaveFragmentReference.class).value() == Integer.MIN_VALUE &&
                    field.getAnnotation(SaveFragmentReference.class).tag().equals("")){
                log.error(checkIfAnnotationParameterIsEmpty(field));
            }
            saveFieldFragmentReferences.add(field);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            saveStateInjector.restoreFields(savedInstanceState, "");
            restoreFragmentReferences();
        }
        singletonInjector.processSingletons(getApplicationContext());
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        viewInjector.injectViewList(this);
    }

    private void restoreFragmentReferences() {
        for (Field field:  saveFieldFragmentReferences){
            if (field.getAnnotation(SaveFragmentReference.class).tag().equals("")){
                restoreFragmentReferenceById(field);
            } else {
                restoreFragmentReferenceByTag(field);
            }
        }
    }

    private void restoreFragmentReferenceById(Field field) {
        try {
            int fragmentId = field.getAnnotation(SaveFragmentReference.class).value();
            Object object = getSupportFragmentManager().findFragmentById(fragmentId);
            if (object == null){
                log.error(checkAnnotationParameterIdMessage(this, getResources(), field));
            }
            field.set(this,object);
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void restoreFragmentReferenceByTag(Field field) {
        try {
            String fragmentTag = field.getAnnotation(SaveFragmentReference.class).tag();
            Object object = getSupportFragmentManager().findFragmentByTag(fragmentTag);
            if (object == null){
                log.error(checkAnnotationParameterIdMessageByTag(this, field));
                return; // we return because sometimes it's not an error, but we notify it.
            }
            field.set(this,object);
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        saveStateInjector.storeFields(savedInstanceState);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    public void subcribeOnBackPressed(OnBackPressedListener fragment){
        onBackPressedListeners.add(fragment);
    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListeners.isEmpty()){
            super.onBackPressed();
        }

        boolean finish = false;
        Iterator<OnBackPressedListener> iterator = onBackPressedListeners.iterator();
        while (iterator.hasNext() && !finish){
            OnBackPressedListener listener = iterator.next();
            finish = listener.onBackPressed();
        }
    }

    public void goBack(){
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        log.debug("Destroying sloth action bar activity");
        saveFieldFragmentReferences.clear();
        onBackPressedListeners.clear();
    }

    public void unsubscribe (SlothComponent slothFragment){
        onBackPressedListeners.remove(slothFragment);
    }

    @Override
    public Activity getSlothActivity() {
        return this;
    }
}
