package es.slothdevelopers.slothframework.utils;

import android.content.res.Resources;

import java.lang.reflect.Field;

import es.slothdevelopers.slothframework.annotations.InjectView;
import es.slothdevelopers.slothframework.annotations.SaveFragmentReference;
import es.slothdevelopers.slothframework.annotations.Singleton;

public class MessageUtils {

    public static String checkSingletonParameter(Field field) {
        return
                "\n***********************************************************************" +
                "\nDid you forget to put the parameter in the Singleton annotation??" +
                "\nField: " + field.getName() + ", Type:" + field.getType().getSimpleName() +
                "\nExample:\n" +
                "@Singleton(\"singletonReferenceExapleName\")\n" +
                field.getType().getSimpleName() + " " + field.getName() + ";" +
                "\n***********************************************************************";
    }

    public static String checkAnnotationParameterIdMessage(Object caller, Resources resources, Field field) {
        int fragmentId = field.getAnnotation(SaveFragmentReference.class).value();
        String resString = "R." + resources.getResourceTypeName(fragmentId) + "." +
                resources.getResourceEntryName(fragmentId);
        return
                "\n***********************************************************************" +
                "\nIn the class: " + caller.getClass().getSimpleName() +
                "\nDid you put the Right  parameter in the SaveFragmentStateAnnotation??" +
                "\nWe can not find the fragment by the id: " + resString +
                "\nField: " + field.getName() + ", Type:" + field.getType().getSimpleName() +
                "\nPLEASE CHECK:\n" +
                "@SaveFragmentReference(" + resString + ")\n" +
                field.getType().getSimpleName() + " " + field.getName() + ";" +
                "\nThis also can be caused becouse the fragment haven't been initializated" +
                "\nor never used. Be careful." +
                "\n***********************************************************************";
    }

    public static String checkAnnotationParameterIdMessageByTag(Object caller, Field field) {
        String fragmentTag = field.getAnnotation(SaveFragmentReference.class).tag();

        return
                "\n***********************************************************************" +
                "\nIn the class: " + caller.getClass().getSimpleName() +
                "\nDid you put the Right  parameter in the SaveFragmentStateAnnotation??" +
                "\nWe can not find the fragment by the tag: " + fragmentTag +
                "\nField: " + field.getName() + ", Type:" + field.getType().getSimpleName() +
                "\nPLEASE CHECK:\n" +
                "@SaveFragmentReference(tag=" + fragmentTag + ")\n" +
                field.getType().getSimpleName() + " " + field.getName() + ";" +
                "\nThis also can be caused becouse the fragment haven't been initializated" +
                "\nor never used. Be careful." +
                "\n***********************************************************************";
    }

    public static String errorInjectingView(Field field, Resources resources) {
        int fragmentId = field.getAnnotation(InjectView.class).value();
        String resString = "R." + resources.getResourceTypeName(fragmentId) + "." +
                resources.getResourceEntryName(fragmentId);
        return
                "\n***********************************************************************" +
                "\nError injecting view in: " + field.getDeclaringClass().getSimpleName() + "." +
                "\nIt's possible that the type you are trying to inject differs from " +
                "the one declared in the layout.\nCheck code.\n" +
                "@InjectView("+resString + ")\n" +
                field.getType().getSimpleName() + " " + field.getName() + ";" +
                "\n***********************************************************************";
    }

    public static String errorObtainingViewIdToInject(Field field) {
        return
                "\n***********************************************************************" +
                "\nIn " + field.getDeclaringClass().getSimpleName() + ":" +
                "\nDid you forget to put the parameter in the InjectView annotation??" +
                "\nField: " + field.getName() + ", Type:" + field.getType().getSimpleName() +
                "\nExample:\n" +
                "@InjectView(R.id.exampleView)\n" +
                field.getType().getSimpleName() + " " + field.getName() + ";" +
                "\n***********************************************************************";
    }

    public static String checkIfAnnotationParameterIsEmpty(Field field) {
        return
                "\n***********************************************************************" +
                "\nIn " + field.getDeclaringClass().getSimpleName() + ":" +
                "\nDid you forget to put the parameter in the SaveFragmentStateAnnotation??" +
                "\nField: " + field.getName() + ", Type:" + field.getType().getSimpleName() +
                "\nExample:\n" +
                "@SaveFragmentReference(R.id.fragmentExampleId)\n" +
                field.getType().getSimpleName() + " " + field.getName() + ";" +
                "\n or " +
                "\n@SaveFragmentReference(tag=\"ExampleTag\")\n" +
                field.getType().getSimpleName() + " " + field.getName() + ";" +
                "\n***********************************************************************";
    }

    public static String checkSlothApplicationMessage() {
        return
                "\n***********************************************************************" +
                "\nIn order to use the Singleton annotation you must declare your application" +
                "\nas a SlothApplication." +
                "\nTo do it go to your AndroidManifest.xml and in your application declare" +
                "\n the android name to:" +
                "\n \"es.slothdevelopers.slothframework.application.SlothApplication\"" +
                "\nExample:" +
                "\n<application\n" +
                "\n        android:name=\"es.slothdevelopers.slothframework.application.SlothApplication\"\n" +
                "\n        android:allowBackup=\"true\"\n" +
                "\n        android:icon=\"@drawable/ic_launcher\"\n" +
                "\n        android:label=\"@string/app_name\"\n" +
                "\n        android:theme=\"@style/AppTheme\" >" +
                "\n..." +
                "\n</application>"+
                "\n***********************************************************************";
    }

    public static String checkParentIsSloth(String parentClassName) {
        return
            "\n***********************************************************************" +
            "\nIn order to use SlothFragments your container activity, in this case" +
            "\n\"" + parentClassName + "\" must extend SlothFragmentActivity or SlothActionBarActivity." +
            "\nExample:" +
            "\nclass " + parentClassName + " extends SlothFragmentActivity " +
            "\nExample:" +
            "\nclass " + parentClassName + " extends SlothActionBarActivity " +
            "\n***********************************************************************";
    }

    public static String checkIfSingletonIsDefined(Field field) {
        return
            "\n***********************************************************************" +
            "\nThe singleton you defined in \"" + field.getDeclaringClass().getSimpleName() + "\" is null." +
            "\nField: " + field.getName() + ", Type:" + field.getType().getSimpleName() +
            "\nMaybe you forget to declare the singleton annotation in another class, or initialize" +
            "\nthe singleton in this class." +
            "\nExample:\nDid you forget to initialize the class?" +
            "\n@Singleton(\"" + field.getAnnotation(Singleton.class).value() + "\")\n" +
            field.getType().getSimpleName() + " " + field.getName() + " = new " + field.getDeclaringClass().getSimpleName() + "();" +
            "\n***********************************************************************";
    }

    public static String adviceSingletonIsPrimitiveType(Field field) {
        String message =
            "\n***********************************************************************" +
            "\nField: " + field.getName() + ", Type:" + field.getType().getSimpleName() +
            "\nThe singleton you defined in \"" + field.getDeclaringClass().getSimpleName() + "\" is a primitive type" +
            "\nor a basic Wrapper such as Integer, Float, Double, etc. or a String." +
            "\nThis are specials Types in Java and are not compatible with Singleton." +
            "\nYou still can use it. You will have access from other classes but if you modify the value" +
            "\nTHE SINGLETON WONT CHANGE and other classes WONT SEE THE CHANGE." +
            "\nIf you want to modify it the singleton you can USE A WRAPPER or your CUSTOM CLASS." +
            "\nWe provide you a generic wrappres in:" +
            "\nes.slothdevelopers.slothframent.wrapper.Wrapper";
        if (!field.getType().isPrimitive()) {
            message += "\nExample" +
                       "\n@Singleton(\"" + field.getAnnotation(Singleton.class).value() + "\")\n" +
                       "\nWrapper<" + field.getType().getSimpleName() + "> " + field.getName() +
                       " = new Wrapper<" + field.getType().getSimpleName() + ">();";
        }
        message += "\n***********************************************************************";

        return message;
    }

    public static String errorInjectingIncompatibleType(Object o, Field field) {
        return
                "\n***********************************************************************" +
                "\nClass: " + field.getDeclaringClass().getSimpleName()  + " Field: " + field.getName() + ", Type:" + field.getType().getSimpleName() +
                "\nYou are trying to inject an incompatible type." +
                "\nYour field is a " + field.getType().getSimpleName() + " and " +
                "\nthe object you are trying to inject is " + o.getClass().getSimpleName() +
                "\nMaybe you put the wrong type, or the wrong tag to the singleton annotation" +
                "\nExample:" +
                "\n@Singleton(\"" + field.getAnnotation(Singleton.class).value() + "\")\n" +
                o.getClass().getSimpleName() + " " + field.getName() + " = new " + o.getClass().getSimpleName() + "();" +
                "\n***********************************************************************";
    }

    public static String checkIsSavingStateOfParcelable(Field field) {
            return "\n***********************************************************************" +
                    "\nIn class: \"" + field.getDeclaringClass().getSimpleName() + "\" you are trying to" +
                    "\nsave the state of the field: " + field.getName() + ", Type:" + field.getType().getSimpleName() +
                    "\nThe annotation @SaveState it's not designed to save the state of parcebles (right now)." +
                    "\n***********************************************************************";
    }

    public static String checkActivityObjectImplementsInterface(Field field, Object container) {
       return "\n***********************************************************************" +
              "\nClass: " + field.getDeclaringClass().getSimpleName() + ", Field: " +
              "\n@ImplementedInActivity" +
              "\n" + field.getType().getSimpleName() + " " + field.getName() + ";" +
              "\nYou are trying to attach " + container.getClass().getSimpleName() + " to " +  field.getName() +
              "\nbut " + container.getClass().getSimpleName() + " is not implementing the interface " + field.getType().getSimpleName() +
              "\n" + container.getClass().getSimpleName() + " needs to implement it." +
              "\nExample: " +
              "\npublic class " + container.getClass().getSimpleName() + " implements " + field.getType().getSimpleName() +
              "\n***********************************************************************";
    }

    public static String checkParentFragmentObjectImplementsInterface(Field field, Object container) {
        return "\n***********************************************************************" +
                "\nClass: " + field.getDeclaringClass().getSimpleName() + ", Field: " +
                "\n@ImplementedInParentFragment" +
                "\n" + field.getType().getSimpleName() + " " + field.getName() + ";" +
                "\nYou are trying to attach " + container.getClass().getSimpleName() + " to " +  field.getName() +
                "\nbut " + container.getClass().getSimpleName() + " is not implementing the interface " + field.getType().getSimpleName() +
                "\n" + container.getClass().getSimpleName() + " needs to implement it." +
                "\nExample: " +
                "\npublic class " + container.getClass().getSimpleName() + " implements " + field.getType().getSimpleName() +
                "\n***********************************************************************";
    }
}
