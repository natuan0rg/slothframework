package es.slothdevelopers.slothframework;

import android.app.Activity;

/**
 * Created by Jesus Jimenez on 17/03/14.
 */
public interface SlothComponent {
    Activity getSlothActivity();
}
