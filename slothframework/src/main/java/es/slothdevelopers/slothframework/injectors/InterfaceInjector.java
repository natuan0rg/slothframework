package es.slothdevelopers.slothframework.injectors;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

import es.slothdevelopers.slothframework.annotations.ImplementedInActivity;
import es.slothdevelopers.slothframework.annotations.ImplementedInParentFragment;
import es.slothdevelopers.slothframework.utils.MessageUtils;
import es.slothdevelopers.slothframework.utils.SlothLog;

public class InterfaceInjector {
    List<Field> interfacesToAttachToActivity = new LinkedList<Field>();
    List<Field> interfacesToAttachToParentFragment = new LinkedList<Field>();
    SlothLog log;
    Object caller;

    public InterfaceInjector(Object caller) {
        this.caller = caller;
        this.log = new SlothLog(caller);
    }

    public void checkIfFieldIsInterfaceToBeAttached(Field field) {
        if (field.isAnnotationPresent(ImplementedInActivity.class)){
            interfacesToAttachToActivity.add(field);
        } else if (field.isAnnotationPresent(ImplementedInParentFragment.class)) {
            interfacesToAttachToParentFragment.add(field);
        }
    }

    public void attachInterfacesToActivity(Object container) {
        for (Field field: interfacesToAttachToActivity){
            try {
                field.set(caller, container);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ClassCastException e) {
                throw new ClassCastException(container.toString()
                        + " must implement " + field.getType());
            } catch (IllegalArgumentException e){
                log.error(MessageUtils.checkActivityObjectImplementsInterface(field, container));
            }
        }
    }
    public void attachInterfacesToParentFragment(Object container) {
        for (Field field: interfacesToAttachToParentFragment){
            try {
                field.set(caller, container);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ClassCastException e) {
                throw new ClassCastException(container.toString()
                        + " must implement " + field.getType());
            } catch (IllegalArgumentException e){
                log.error(MessageUtils.checkParentFragmentObjectImplementsInterface(field, container));
            }
        }
    }
}

