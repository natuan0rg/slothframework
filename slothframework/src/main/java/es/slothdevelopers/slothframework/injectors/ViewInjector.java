package es.slothdevelopers.slothframework.injectors;

import android.app.Activity;
import android.view.View;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

import es.slothdevelopers.slothframework.annotations.InjectView;
import es.slothdevelopers.slothframework.utils.SlothLog;

import static es.slothdevelopers.slothframework.utils.MessageUtils.errorInjectingView;
import static es.slothdevelopers.slothframework.utils.MessageUtils.errorObtainingViewIdToInject;

public class ViewInjector {
    SlothLog log;
    List<Field> viewsToInject = new LinkedList<Field>();
    Object caller;

    public ViewInjector(Object caller){
        this.log = new SlothLog(caller);
        this.caller = caller;
    }

    public void checkIfViewToInject(Field field) {
        if (field.isAnnotationPresent(InjectView.class)){
            viewsToInject.add(field);
        }
    }

    public void injectViewList(Activity activity) {
        for (Field field: viewsToInject){
            injectView(activity, field);
        }
    }

    private void injectView(Activity activity, Field field) {
        int id = field.getAnnotation(InjectView.class).value();
        if (id == Integer.MIN_VALUE){
            log.debug(errorObtainingViewIdToInject(field));
        }
        try {
            field.set(caller, activity.findViewById(id));
        } catch (IllegalAccessException e) {
            log.error(errorInjectingView(field, activity.getResources()));
        } catch (IllegalArgumentException ex){
            log.error(errorInjectingView(field, activity.getResources()));
        }
    }

    public void injectViewList(View container) {
        for (Field field: viewsToInject){
            injectView(container, field);
        }
    }

    private void injectView(View view, Field field) {
        int id = field.getAnnotation(InjectView.class).value();
        if (id == Integer.MIN_VALUE){
            log.debug(errorObtainingViewIdToInject(field));
        }
        try {
            field.set(caller, view.findViewById(id));
        } catch (IllegalAccessException e) {
            log.error(errorInjectingView(field, view.getResources()));
        } catch (IllegalArgumentException ex){
            log.error(errorInjectingView(field, view.getResources()));
        }
    }

    public List<Field> getViewsToInject() {
        return viewsToInject;
    }
}
