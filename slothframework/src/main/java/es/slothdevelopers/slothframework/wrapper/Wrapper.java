package es.slothdevelopers.slothframework.wrapper;

public class Wrapper<T> {
    T value;

    public Wrapper() {
    }

    public Wrapper(T value) {

        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
