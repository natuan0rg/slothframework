package es.slothdevelopers.slothframework.injectors;

import android.os.Bundle;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

import es.slothdevelopers.slothframework.annotations.SaveState;
import es.slothdevelopers.slothframework.utils.MessageUtils;
import es.slothdevelopers.slothframework.utils.SlothLog;

public class SaveStateInjector {
    SlothLog log;
    Object caller;
    List<Field> saveFieldParameters = new LinkedList<Field>();

    public SaveStateInjector(Object caller) {
        this.caller = caller;
        this.log = new SlothLog(caller);
    }
    public void checkIfFieldToSaveState(Field field) {
        if (field.isAnnotationPresent(SaveState.class)){
            saveFieldParameters.add(field);
        }
    }

    public void storeFields(Bundle savedInstanceState) {
        for (Field field: saveFieldParameters){
            try {
                storeField(caller, field, savedInstanceState, "");
            } catch (Exception e) {
                log.error(
                        "*********************************** " +
                        "\nField: " + field.getName() + " Class: " + field.getDeclaringClass().getSimpleName() +
                        "\nError storing field." +
                        "\nException:\n" +
                        "***********************************\n" +
                        e.toString()
                );
                e.printStackTrace();
            }
        }
    }

    private void storeField(Object object, Field field, Bundle savedInstanceState, String storeInBundlePrefix) throws Exception {

        if (field.getType().equals(String.class)){
            savedInstanceState.putString(storeInBundlePrefix + field.getName(), (String) field.get(object));
        }else if (field.getType().equals(int.class)){
            savedInstanceState.putInt(storeInBundlePrefix + field.getName(), field.getInt(object));
        } else if (field.getType().equals(boolean.class)){
            savedInstanceState.putBoolean(storeInBundlePrefix + field.getName(), field.getBoolean(object));
        } else if (field.getType().equals(long.class)){
            savedInstanceState.putLong(storeInBundlePrefix + field.getName(), field.getLong(object));
        } else if (field.getType().equals(double.class)) {
            savedInstanceState.putDouble(storeInBundlePrefix + field.getName(), field.getDouble(object));
        } else if (field.getType().equals(float.class)) {
            savedInstanceState.putFloat(storeInBundlePrefix + field.getName(), field.getFloat(object));
        } else if (field.getType().isEnum()){
            Enum<?> e = (Enum<?>) field.get(object);
            savedInstanceState.putString(storeInBundlePrefix + field.getName() + "name", e.name());
            savedInstanceState.putInt(storeInBundlePrefix + field.getName() + "ordinal", e.ordinal());
        } else if (isArrayList(field)) {
            if (isListOfParcelable(field)) {
                savedInstanceState.putParcelableArrayList(storeInBundlePrefix + field.getName(), (java.util.ArrayList<? extends android.os.Parcelable>) field.get(object));
            } else {
                storeListInBundle(field.get(object), field, savedInstanceState, storeInBundlePrefix + field.getName() + ".");
            }
        } else if (implementsList(field.getType())) {
            if (isListOfParcelable(field)){
                storeListOfParcelableInBundle(field.get(object), field, savedInstanceState, storeInBundlePrefix + field.getName() + ".");
            } else {
                storeListInBundle(field.get(object), field, savedInstanceState, storeInBundlePrefix + field.getName() + ".");
            }
        } else{// we suppose it's a pojo
            for (Field objectField: field.get(object).getClass().getDeclaredFields()){
                objectField.setAccessible(true);
                storeField(field.get(object), objectField, savedInstanceState, storeInBundlePrefix + field.getName() + ".");
            }
        }
    }

    private boolean isArrayList(Field field) {
        if (field.getType().getName().equals("java.util.ArrayList")){ // true if it is a list
            return true;
        }
        return false;
    }

    private boolean isListOfParcelable(Field field){

        Type genericFieldType = field.getGenericType();

        if(genericFieldType instanceof ParameterizedType){
            ParameterizedType aType = (ParameterizedType) genericFieldType;
            Type[] fieldArgTypes = aType.getActualTypeArguments();
            for(Type fieldArgType : fieldArgTypes){
                Class fieldArgClass = (Class) fieldArgType;
                System.out.println("fieldArgClass = " + fieldArgClass);
                for (Class c: fieldArgClass.getInterfaces()){
                    if (c.equals(android.os.Parcelable.class)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private  boolean implementsList(Class clazz){
        if (clazz.getName().equals("java.util.List")){ // true if it is a list
            return true;
        }
        for (Class actualClazz : clazz.getInterfaces()){
            if (actualClazz.getName().equals("java.util.List")){
                return true;
            }
        }
        return false;
    }

    private void storeListInBundle(Object object, Field field, Bundle savedInstanceState, String storeInBundlePrefix) throws Exception {
        List<Object> list = (List<Object>) object;
        savedInstanceState.putInt(storeInBundlePrefix + "size", list.size());
        savedInstanceState.putString(storeInBundlePrefix + "listType", object.getClass().getCanonicalName());
        if (list.size()>0){
            Object element = list.get(0);
            savedInstanceState.putString(storeInBundlePrefix + "type", element.getClass().getCanonicalName());
        }
        long index = 0;
        for(Object element: list){
            for (Field elementField: element.getClass().getDeclaredFields()){
                elementField.setAccessible(true);
                storeField(element, elementField, savedInstanceState, storeInBundlePrefix + index + ".");
            }
            index++;
        }
    }
    private void storeListOfParcelableInBundle(Object object, Field field, Bundle savedInstanceState, String storeInBundlePrefix) throws IllegalAccessException {
        List<Object> list = (List<Object>) object;
        savedInstanceState.putInt(storeInBundlePrefix + "size", list.size());
        savedInstanceState.putString(storeInBundlePrefix + "listType", object.getClass().getCanonicalName());
        if (list.size()>0){
            Object element = list.get(0);
            savedInstanceState.putString(storeInBundlePrefix + "type", element.getClass().getCanonicalName());
        }
        long index = 0;
        for(Object element: list){
            savedInstanceState.putParcelable(storeInBundlePrefix + index, (android.os.Parcelable) element);
            index++;
        }
    }


    public void restoreFields(Bundle savedInstanceState, String recoverFromBundlePrefix) {
        for (Field field: saveFieldParameters){
            try {
                restoreField(caller, field, savedInstanceState, recoverFromBundlePrefix);
            } catch (IllegalAccessException e) {
                log.error(
                        "*********************************** "+
                        "\nField: " + field.getName() + " Class: " + field.getDeclaringClass().getSimpleName() +
                        "\nError storing field." +
                        "\nIllegal access exception:\n " +
                        "***********************************\n" +
                        e.toString());
                e.printStackTrace();
            }catch (ClassNotFoundException e) {
                log.error(
                        "*********************************** "+
                        "\nField: " + field.getName() + " Class: " + field.getDeclaringClass().getSimpleName() +
                        "\nError storing field." +
                        "\nClasNotFoundException:\n" +
                        "***********************************\n" +
                        e.toString());
                e.printStackTrace();
            } catch (InstantiationException e) {
                log.error(
                        "*********************************** "+
                        "\nField: " + field.getName() + " Class: " + field.getDeclaringClass().getSimpleName() +
                        "\nError storing field." +
                        "\nInstantiation Exception:\n" +
                        "***********************************\n" +
                        e.toString());
                e.printStackTrace();
            } catch (Exception e) {
                log.error(
                        "*********************************** "+
                        "\nField: " + field.getName() + " Class: " + field.getDeclaringClass().getSimpleName() +
                        "\nError storing field." +
                        "\nException restoring field:\n" +
                        "***********************************\n" +
                        e.toString());
                e.printStackTrace();
            }
        }
    }

    private void restoreField(Object caller, Field field, Bundle savedInstanceState, String restoreFromBundlePrefix) throws Exception {
        if (field.getType().equals(String.class)){
            field.set(caller, savedInstanceState.getString(restoreFromBundlePrefix + field.getName()));
        } else if (field.getType().equals(int.class)){
            field.setInt(caller, savedInstanceState.getInt(restoreFromBundlePrefix + field.getName()));
        } else if (field.getType().equals(boolean.class)){
            field.setBoolean(caller, savedInstanceState.getBoolean(restoreFromBundlePrefix + field.getName()));
        } else if (field.getType().equals(long.class)){
            field.setLong(caller, savedInstanceState.getLong(restoreFromBundlePrefix + field.getName()));
        } else if (field.getType().equals(double.class)) {
            field.setDouble(caller, savedInstanceState.getDouble(restoreFromBundlePrefix + field.getName()));
        } else if (field.getType().equals(float.class)) {
            field.setFloat(caller, savedInstanceState.getFloat(restoreFromBundlePrefix + field.getName()));
        } else if (field.getType().isEnum()){
            field.set(caller, Enum.valueOf((Class) field.getType(), savedInstanceState.getString(restoreFromBundlePrefix + field.getName() + "name")));
        } else if (isArrayList(field)) {
            if (isListOfParcelable(field)) {
                field.set(caller, savedInstanceState.getParcelableArrayList(restoreFromBundlePrefix + field.getName()));
            } else {
                String listType = savedInstanceState.getString(restoreFromBundlePrefix + field.getName() + "." + "listType");
                Object obj = Class.forName(listType).newInstance();
                restoreListFromBundle(obj, savedInstanceState, restoreFromBundlePrefix + field.getName() + ".");
                field.set(caller, obj);
            }
        } else if (implementsList(field.getType())) {
            if (isListOfParcelable(field)){
                String listType = savedInstanceState.getString(restoreFromBundlePrefix + field.getName() + "." + "listType");
                Object obj = Class.forName(listType).newInstance();
                restoreListOfParcelableFromBundle(obj, savedInstanceState, restoreFromBundlePrefix + field.getName() + ".");
                field.set(caller, obj);
            } else {
                String listType = savedInstanceState.getString(restoreFromBundlePrefix + field.getName() + "." + "listType");
                Object obj = Class.forName(listType).newInstance();
                restoreListFromBundle(obj, savedInstanceState, restoreFromBundlePrefix + field.getName() + ".");
                field.set(caller, obj);
            }
        } else if (field.getType().getName().equals("android.os.Parcelable$Creator")) {
            log.warn(MessageUtils.checkIsSavingStateOfParcelable(field));
        }else {
            Object obj = Class.forName(field.getType().getName()).newInstance();
            for (Field objectField: field.getType().getDeclaredFields()){
                objectField.setAccessible(true);
                restoreField(obj, objectField, savedInstanceState, restoreFromBundlePrefix + field.getName() + ".");
            }
            field.set(caller, obj);
        }
    }

    private void restoreListOfParcelableFromBundle(Object object, Bundle savedInstanceState, String restoreFromBundlePrefix) {
        List<Object> list = (List<Object>) object;

        int size = savedInstanceState.getInt(restoreFromBundlePrefix + "size");
        String type = savedInstanceState.getString(restoreFromBundlePrefix + "type");
        for(int i = 0; i < size; i++) {

            Object obj = savedInstanceState.getParcelable(restoreFromBundlePrefix + i);
            list.add(obj);
        }
    }

    private void restoreListFromBundle(Object object, Bundle savedInstanceState, String restoreFromBundlePrefix) throws Exception {
        List<Object> list = (List<Object>) object;

        int size = savedInstanceState.getInt(restoreFromBundlePrefix + "size");
        String type = savedInstanceState.getString(restoreFromBundlePrefix + "type");
        for(int i = 0; i < size; i++) {

            Object obj = Class.forName(type).newInstance();

            for (Field elementField: obj.getClass().getDeclaredFields()){
                elementField.setAccessible(true);
                restoreField(obj, elementField, savedInstanceState, restoreFromBundlePrefix + i + ".");
            }
            list.add(obj);
        }
    }

}
