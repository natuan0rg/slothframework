package es.slothdevelopers.slothframework.storage;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesHelper {
    private static String DEFAULT_STRING_VALUE = "";
    private static boolean DEFAULT_BOOL_VALUE = false;
    private static int DEFAULT_INT_VALUE = 0;

    private SharedPreferences sharedPreferences;

    public SharedPreferencesHelper(Context context, String name) {
        sharedPreferences = getSharedPreferences(context, name);
    }

    private SharedPreferences getSharedPreferences(Context context, String name){
        return context.getSharedPreferences(name, 0);
    }

    public boolean getBoolean(String nameField){
        return sharedPreferences.getBoolean(nameField, DEFAULT_BOOL_VALUE);
    }

    public void setBoolean(String nameField, boolean value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(nameField, value);
        editor.commit();
    }

    public String getString(String nameField){
        return sharedPreferences.getString(nameField, DEFAULT_STRING_VALUE);
    }

    public void setString(String nameField, String value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(nameField, value);
        editor.commit();
    }

    public int getInteger(String nameField){
        return sharedPreferences.getInt(nameField, DEFAULT_INT_VALUE);
    }

    public void setInteger(String nameField, int value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(nameField, value);
        editor.commit();
    }

    public int getFloat(String nameField){
        return sharedPreferences.getInt(nameField, DEFAULT_INT_VALUE);
    }

    public void setFloat(String nameField, float value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(nameField, value);
        editor.commit();
    }
}