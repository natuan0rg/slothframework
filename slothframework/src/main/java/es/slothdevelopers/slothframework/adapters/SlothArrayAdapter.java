package es.slothdevelopers.slothframework.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.slothdevelopers.slothframework.injectors.SingletonInjector;
import es.slothdevelopers.slothframework.injectors.ViewInjector;
import es.slothdevelopers.slothframework.utils.SlothLog;

public class SlothArrayAdapter<T> extends ArrayAdapter<T> {
    ViewInjector viewInjector = new ViewInjector(this);
    SingletonInjector singletonInjector = new SingletonInjector(this);

    protected Context mContext;
    protected int layoutResourceId;
    protected SlothLog log;
    protected List<T> data = null;

    boolean singletonsAreProcessed = false;

    public SlothArrayAdapter(Context mContext, int layoutResourceId, List<T> data) {
        super(mContext, layoutResourceId, data);
        log = new SlothLog(this);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;

        parseFields();

        // If we process the singletons here we will only have access to already created singletons,
        // the new ones will not get initializated because this constructor goes first than the child.
        // ONLY READ SINGLETONS ...
    }



    private void parseFields() {
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            viewInjector.checkIfViewToInject(field);
            singletonInjector.checkIfFieldIsSingleton(field);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        processSingletons();

        convertView = viewHolderPattern(convertView, parent);

        checkIfLastElementForPosition(position);

        onCreateViewForPosition(convertView, position, data.get(position));

        return convertView;
    }

    private void processSingletons() {
        if (!singletonsAreProcessed){
            singletonInjector.processSingletons(getContext().getApplicationContext());
            singletonsAreProcessed = true;
        }
    }

    /**
     * Based on:
     * http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/
     */
    private View viewHolderPattern(View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
            viewInjector.injectViewList(convertView);
            storeReferencesInViewHolder(convertView);

        } else {
            restoreReferencesFromViewHolder(convertView);
        }
        return convertView;
    }

    private void storeReferencesInViewHolder(View convertView) {
        List<Field> fieldList = viewInjector.getViewsToInject();
        Map<Field,Object> objectList = new HashMap<Field, Object>();

        for(Field field: fieldList){
            try {
                Object object = field.get(this);
                objectList.put(field, object);
            } catch (IllegalAccessException e) {
                log.error("Can't store preference");
                e.printStackTrace();
            }
        }
        convertView.setTag(objectList);
    }

    private void restoreReferencesFromViewHolder(View convertView) {
        Map<Field,Object> objectList = (Map<Field, Object>) convertView.getTag();
        for(Field field: objectList.keySet()){
            try {
                field.set(this, objectList.get(field));
            } catch (IllegalAccessException e) {
                log.error("Can't restore reference");
                e.printStackTrace();
            }
        }
    }

    protected void onCreateViewForPosition(View viewCreated, int position, T data){

    }

    private void checkIfLastElementForPosition(int position) {
        if (position == (data.size() - 1)){
            onCreateLastElementOfList();
        }
    }

    protected void onCreateLastElementOfList(){
    }

}
