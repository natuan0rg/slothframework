package es.slothdevelopers.slothframework.utils;

import android.util.Log;

public class SlothLog {
    private final String className;;

    public SlothLog(Object caller){
        this.className = caller.getClass().getName();
    }

    public void debug(String message){
        Log.d(className, ".\n" + message);
    }
    public void debug(int message){Log.d(className, ".\n" + message);}
    public void warn(String message){
        Log.w(className, ".\n" + message);
    }
    public void warn(int message){Log.w(className, ".\n" + message);}
    public void error(String message){
        Log.e(className, ".\n" + message);
    }
    public void error(int message){
        Log.e(className, ".\n" + message);
    }
}
